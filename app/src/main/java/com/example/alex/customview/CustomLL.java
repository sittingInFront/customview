package com.example.alex.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 06.09.2017.
 */

public class CustomLL extends LinearLayout implements View.OnClickListener {

    OnCustomClickListener onCustomClickListener;

    private final String DIRECTION_LEFT = "left";
    private final String DIRECTION_RIGHT = "right";
    private final String DIRECTION_UP = "up";
    private final String DIRECTION_DOWN = "down";
    private final int DEFAULT_SPEED = 50;

    private final int defaultLayoutID = R.layout.custom_layout;
    View selectedView;
    private Paint mPaint;
    private Context context;

    private int mWidth;
    private int mHeight;
    private boolean mAnimationFlag = false;
    private int mMovingX;
    private int mMovingY;
    private int mSpeed;
    private String mDirection;

    private int mCircleRadius = 20;


    public void setSpeed(int mSpeed) {
        this.mSpeed = mSpeed;
    }


    public void setCircleRadius(int mCircleRadius) {
        this.mCircleRadius = mCircleRadius;
    }

    public CustomLL(Context context) {
        super(context);
    }

    public CustomLL(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateView(context, attrs);
        paintInit();
        this.context = context;
    }

    public CustomLL(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflateView(context, attrs);
        paintInit();
        this.context = context;

    }

    private void inflateView(Context context, AttributeSet attrs) {
        TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Customll, 0, 0);
        int layoutID = attributes.getResourceId(R.styleable.Customll_custom_layout, defaultLayoutID);
        this.mSpeed = attributes.getResourceId(R.styleable.Customll_speed, DEFAULT_SPEED);
        attributes.recycle();

        ViewGroup layout = (ViewGroup) LayoutInflater.from(context).inflate(layoutID, this, true);
        parseView(layout, 1);
        setWillNotDraw(false);
    }

    private void parseView(ViewGroup layout, int j) {
        for (int i = 0; i < layout.getChildCount(); i++) {
            if (layout.getChildAt(i) instanceof ViewGroup) {
                parseView((ViewGroup) layout.getChildAt(i), j);
            } else if (layout.getChildAt(i) instanceof ImageView) {
                layout.getChildAt(i).setTag(j);
                layout.getChildAt(i).setOnClickListener(this);
                j++;
            }
        }
    }

    @Override
    public void onClick(View view) {
        view.setBackgroundColor(Color.RED);
        if (selectedView != null) {
            selectedView.setBackgroundColor(Color.TRANSPARENT);
        }
        selectedView = view;

        if (onCustomClickListener != null) {
            onCustomClickListener.OnSelected((int) view.getTag());
        }

        switch (view.getId()) {
            case R.id.iwLeft:
                mMovingX = mWidth + mCircleRadius;
                mMovingY = mCircleRadius + 10;
                mDirection = DIRECTION_LEFT;
                break;
            case R.id.iwRight:
                mMovingX = -mCircleRadius;
                mMovingY = mHeight - mCircleRadius - 10;
                mDirection = DIRECTION_RIGHT;
                break;
            case R.id.iwUp:
                mMovingX = mCircleRadius + 10;
                mMovingY = mHeight + mCircleRadius;
                mDirection = DIRECTION_UP;
                break;
            case R.id.iwDown:
                mMovingX = mWidth - mCircleRadius - 10;
                mMovingY = -mCircleRadius;
                mDirection = DIRECTION_DOWN;
                break;
        }
        mAnimationFlag = true;
        invalidate();
    }

    public void setOnCustomClickListener(OnCustomClickListener onCustomClickListener) {
        this.onCustomClickListener = onCustomClickListener;
    }

    public interface OnCustomClickListener {
        void OnSelected(int id);
    }


    private void paintInit() {
        mPaint = new Paint();
        mPaint.setColor(Color.BLUE);
        mPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (mAnimationFlag == true) {
            try {
                moveCircle(canvas);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int desiredHeight = 200;
        int desiredWidth = 200;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        mWidth = getMeasuredWidth();
        mHeight = getMeasuredHeight();
    }
    
    private void moveCircle(Canvas canvas) throws InterruptedException {
        canvas.drawCircle(mMovingX, mMovingY, mCircleRadius, mPaint);
        switch (mDirection) {
            case DIRECTION_LEFT:
                mMovingX--;
                if (mMovingX <  (-mCircleRadius)) {
                    mAnimationFlag = false;
                }
                break;
            case DIRECTION_RIGHT:
                mMovingX++;
                if (mMovingX >= mWidth + mCircleRadius) {
                    mAnimationFlag = false;
                }
                break;
            case DIRECTION_UP:
                mMovingY--;
                if (mMovingY <= (-mCircleRadius)) {
                    mAnimationFlag = false;
                }
                break;
            case DIRECTION_DOWN:
                mMovingY++;
                if (mMovingY >= mHeight + mCircleRadius) {
                    mAnimationFlag = false;
                }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        try {
            if (mAnimationFlag == true) {
                TimeUnit.MILLISECONDS.sleep(mSpeed);
                invalidate();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
