package com.example.alex.customview;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements CustomLL.OnCustomClickListener {

    CustomLL customLL;
    TextView tvText;
    TextInputEditText tietSpeed;
    Button btnSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        customLL = (CustomLL) findViewById(R.id.llCustom);
        tvText = (TextView) findViewById(R.id.tvText);
        tietSpeed = (TextInputEditText) findViewById(R.id.tietSpeed);
        btnSet = (Button) findViewById(R.id.btnSet);
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tietSpeed.getText().toString() != "") {
                    customLL.setSpeed(Integer.valueOf(tietSpeed.getText().toString()));
                }
            }
        });


        customLL.setOnCustomClickListener(this);
    }

    @Override
    public void OnSelected(int id) {
        switch (id) {
            case 1:
                tvText.setText("left");
                break;
            case 2:
                tvText.setText("right");
                break;
            case 3:
                tvText.setText("up");
                break;
            case 4:
                tvText.setText("down");
                break;
        }
    }
}
